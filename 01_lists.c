#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// определяем элемент списка
typedef struct node {
	int value;          // значение, которое хранит узел 
	struct node *next;  // ссылка на следующий элемент списка
} node;

// определяем сам список
typedef struct list {
	struct node *head;  // начало списка
	struct node *tail;
	struct node *current;
} list;


// инициализация пустого списка
void init(list *l)
{
  l->head = NULL;
}

// удалить все элементы из списка
void clean(list *l)
{
	while (l->head != NULL)
	{
		l->current = l->head;
		l->head = l->head->next;
		free(l->current);
	}
}

// проверка на пустоту списка
bool is_empty(list *l)
{
	return (l->head) == NULL;
}

// поиск элемента по значению. вернуть NULL если элемент не найден
node *find(list *l, int value)
{
	l->current = l->head;
	if (l->head == NULL)
	{
		return NULL;
	}
	while (l->current->value != value)
	{
		if (l->current->next == NULL)
		{
			printf("0 ");
			return NULL;
		}
		else
		{
			l->current = l->current->next;
		}
	}
	printf("1 ");
}

// вставка значения в конец списка, вернуть 0 если успешно
int push_back(list *l, int value)
{
	l->tail = l->head;
	if (l->tail != NULL)
	{
		while (l->tail->next != NULL)
		{
			l->tail = l->tail->next;
		}
		l-> current = malloc(sizeof(node));
        	l->current->value = value;
        	l->current->next = NULL;
		l->tail->next = l->current;
		return 0;
		}
	else
	{
		l->current = malloc(sizeof(node));
        	l->current->value = value;
        	l->current->next = l->head;
        	l->head = l->current;
	}
}

// вставка значения в начало списка, вернуть 0 если успешно
int push_front(list *l, int value)
{
	l->current = malloc(sizeof(node));
	l->current->value = value;
	l->current->next = l->head;
	l->head = l->current;
	return 0;
}

// вставка значения после указанного узла, вернуть 0 если успешно
int insert_after(list *l, unsigned int number, int value)
{
	l->tail = l->head;
	int count = 1;
	while (count < number && l->tail->next !=NULL)
	{
		l->tail = l->tail->next;
		count++;
	}
	l->current = malloc(sizeof(node));
	l->current->value = value;
	l->current->next = l->tail->next;
	l->tail->next = l->current;
	return 0;
}

// удалить первый элемент из списка с указанным значением, 
// вернуть 0 если успешно
int remove_node(list *l, int value)
{
	l->tail = l->head;
	l->current = NULL;
	if (l->head == NULL)
	{
		return 0;
	}
	while (l->tail->value != value)
	{
		if (l->tail->next == NULL)
		{
			return 0;
		}
		else
		{
			l->current = l->tail;
			l->tail = l->tail->next;
		}
	}
	if (l->tail == l->head)
	{
		l->head = l->head->next;
	}
	else
	{
		l->current->next = l->tail->next;
	}
	return 0;
}

// вывести все значения из списка в прямом порядке через пробел,
// после окончания вывода перейти на новую строку
void print(list *l)
{
	l->current = l->head;
	while (l->current != NULL)
	{
		printf("%d", l->current->value);
		printf(" ");
		l->current = l->current->next;
	}
	printf("\n");
}


int main() {
	// Место для вашего кода
	unsigned int n, i;
	int a, k1, k2, z, k3, m, t, j, x;
	struct list mylist;
	(void)scanf("%u", &n);
	init(&mylist);
	for (i=0; i<n; i++)
	{
		scanf("%d", &a);
		push_back(&mylist, a);
	}
	print(&mylist);
	(void)scanf("%d%d%d", &k1, &k2, &k3);
	find(&mylist, k1);
	find(&mylist, k2);
	find(&mylist, k3);
	printf("\n");
	scanf("%d", &m);
	push_back(&mylist, m);
	print(&mylist);
	scanf("%d", &t);
	push_front(&mylist, t);
	print(&mylist);
	(void)scanf("%d%d", &j, &x);
	insert_after(&mylist, j, x);
	print(&mylist);
	scanf("%d", &z);
	remove_node(&mylist, z);
	print(&mylist);
	clean(&mylist);
	return 0;
};
