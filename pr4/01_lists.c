#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


typedef struct node {
	int value;          // значение, которое хранит узел 
	struct node *next;  // ссылка на следующий элемент списка
	struct node *prev;  // ссылка на предыдущий элемент списка
} node;

typedef struct list {
	struct node *head;  // начало списка
	struct node *current;
	struct node *counter;
	struct node *tail;  // конец списка
} list;


// инициализация пустого списка
void init(list *l)
{
	l->head = NULL;
	l->tail = NULL;
}

// удалить все элементы из списка
void clean(list *l)
{
        while (l->head != NULL)
        {
                l->current = l->head;
                l->head = l->head->next;
                free(l->current);
        }
}

// проверка на пустоту списка
bool is_empty(list *l)
{
	return l->head == NULL;
}

// поиск элемента по значению. вернуть NULL если элемент не найден
node *find(list *l, int value)
{
        l->current = l->head;
        if (l->head == NULL)
        {
                return NULL;
        }
        while (l->current->value != value)
        {
                if (l->current->next == NULL)
                {
                        printf("0 ");
                        return NULL;
                }
                else
                {
                        l->current = l->current->next;
                }
        }
        printf("1 ");
}

// вставка значения в конец списка, вернуть 0 если успешно
int push_back(list *l, int value)
{
	l->current = malloc(sizeof(node));
	l->current->value = value;
	l->current->next = NULL;
	l->current->prev = l->tail;
	if (l->tail)
	{
		l->tail->next = l->current;
	}
	l->tail = l->current;
	if (l->head == NULL)
	{
		l->head = l->current;
	}
}

// вставка значения в начало списка, вернуть 0 если успешно
int push_front(list *l, int value)
{
	l->current = malloc(sizeof(node));
	l->current->value = value;
	l->current->next = l->head;
	l->current->prev = NULL;
	if (l->head)
	{
		l->head->prev=l->current;
	}
	if (l->tail == NULL)
	{
		l->tail = l->current;
	}
	l->head = l->current;
	return 0;
}

// вставка значения после указанного узла, вернуть 0 если успешно
int insert_after(list *l, unsigned int number, int value)
{
        l->counter = l->head;
        int count = 1;
        while (count < number && l->counter->next !=NULL)
        {
                l->counter = l->counter->next;
                count++;
        }
        l->current = malloc(sizeof(node));
        l->current->value = value;
        if (l->counter != l->tail)
	{
		l->current->next = l->counter->next;
		l->current->prev = l->counter;
        	l->counter->next = l->current;
		l->current->next->prev = l->current;
        }
	else
	{
		l->counter->next = l->current;
		l->current->prev = l->tail;
		l->current->next = NULL;
		l->tail = l->current;
	}
	return 0;
}

// вставка значения перед указанным узлом, вернуть 0 если успешно
int insert_before(list *l, unsigned int number, int value)
{
	l->counter = l->head;
        int count = 1;
        while (count < number && l->counter->next !=NULL)
        {
                l->counter = l->counter->next;
                count++;
        }
        l->current = malloc(sizeof(node));
        l->current->value = value;
	if (l->counter != l->head)
	{
        	l->current->next = l->counter;
        	l->current->prev = l->counter->prev;
        	l->counter->prev = l->current;
        	l->current->prev->next = l->current;
        }
	else
        {
                l->head->prev = l->current;
                l->current->next = l->head;
                l->current->prev = NULL;
                l->head = l->current;
        }
	return 0;
}

// удалить первый элемент из списка с указанным значением, 
// вернуть 0 если успешно
int remove_first(list *l, int value)
{
	l->counter = l->head;
	if (l->head == NULL)
	{
		return 0;
	}
	while (l->counter->value != value)
	{
		if (l->counter->next == NULL)
		{
			return 0;
		}
		else
		{
			l->current = l->counter;
			l->counter = l->counter->next;
		}
	}
	if (l->counter == l->head)
	{
		l->head = l->head->next;
		l->head->prev = NULL;
		free(l->counter);
		return 0;
	}
	if (l->counter == l->tail)
	{
		l->tail = l->tail->prev;
		l->tail->next = NULL;
		free(l->counter);
		return 0;
	}
	else
	{
		l->current->next = l->counter->next;
		l->counter->next->prev = l->current;
		free(l->counter);
		return 0;
	}
}

// удалить последний элемент из списка с указанным значением, 
// вернуть 0 если успешно
int remove_last(list *l, int value)
{

	l->counter = l->tail;
	if (l->tail == NULL)
	{
		return 0;
	}
	while (l->counter->value != value)
	{
		if (l->counter->prev == NULL)
		{
			return 0;
		}
		else
		{
			l->current = l->counter;
			l->counter = l->counter->prev;
		}
	}
	if (l->counter == l->tail)
	{
		l->tail = l->tail->prev;
		l->tail->next = NULL;
		free(l->counter);
		return 0;
	}
	if (l->counter == l->head)
	{
		l->head = l->head->next;
		l->head->prev = NULL;
		free(l->counter);
		return 0;
	}
	else
	{
		l->current->prev = l->counter->prev;
		l->counter->prev->next = l->current;
		free(l->counter);
		return 0;
	}
}

// вывести все значения из списка в прямом порядке через пробел,
// после окончания вывода перейти на новую строку
void print(list *l)
{
        l->current = l->head;
        while (l->current != NULL)
        {
                printf("%d", l->current->value);
                printf(" ");
                l->current = l->current->next;
        }
        printf("\n");
}


// вывести все значения из списка в обратном порядке через пробел,
// после окончания вывода перейти на новую строку
void print_invers(list *l)
{
        l->current = l->tail;
        while (l->current != NULL)
        {
                printf("%d", l->current->value);
                printf(" ");
                l->current = l->current->prev;
        }
        printf("\n");
}

int main() {
	unsigned int n, i;
        int m, j, x, t, a, r, k1, z, k2, k3, u, y;
	struct list mylist;
	init(&mylist);
	(void)scanf("%u", &n);
	for (i=0; i<n; i++)
        {
                scanf("%d", &a);
                push_back(&mylist, a);
        }
	print(&mylist);
	(void)scanf("%d%d%d", &k1, &k2, &k3);
        find(&mylist, k1);
        find(&mylist, k2);
        find(&mylist, k3);
	printf("\n");
        scanf("%d", &m);
        push_back(&mylist, m);
        print_invers(&mylist);
        scanf("%d", &t);
        push_front(&mylist, t);
        print(&mylist);
	(void)scanf("%d%d", &j, &x);
	insert_after(&mylist, j, x);
	print_invers(&mylist);
	(void)scanf("%d%d", &u, &y);
        insert_before(&mylist, u, y);
	print(&mylist);
	scanf("%d", &z);
        remove_first(&mylist, z);
	print_invers(&mylist);
	scanf("%d", &r);
        remove_last(&mylist, r);
	print(&mylist);
	clean(&mylist);
	return 0;
};
